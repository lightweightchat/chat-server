#pragma once
#include "NetworkFactory.h"
#include "HeaderResolver.h"
#include "HeaderSerializer.h"
#include "Encryptor.h"
#include "clientInfo.h"
#include <functional>
#include <memory>
#include <string>
#include <unordered_map>

using onConnectionCallback = std::function<void(ClientInfo clientInfo)>;
using onDisconnectCallback = std::function<void(ClientInfo clientInfo)>;

class IServer
{
public:
    IServer() = default;
    virtual ~IServer() = default;

    virtual void start() = 0;
    virtual void stop() = 0;
    virtual void ping() = 0;

public:
    void setErrorCallback(onErrorCallback callback)
    {
        m_errorCallback = callback;
    }

    void setConnectionCallback(onConnectionCallback callback)
    {
        m_connectionCallback = callback;
    }

    void setDisconnectCallback(onDisconnectCallback callback)
    {
        m_disconnectCallback = callback;
    }

protected:
    onErrorCallback m_errorCallback;
    onConnectionCallback m_connectionCallback;
    onDisconnectCallback m_disconnectCallback;
};

class Server : public IServer
{

public:
    Server(std::string serverName, IP version, int port, std::shared_ptr<INetworkFactory> networkFactory, std::shared_ptr<ISerializer> serializer = nullptr, std::shared_ptr<IResolver> resolver = nullptr, std::shared_ptr<IEncryptor> encryptor = nullptr);

    virtual ~Server() = default;

    virtual void start() override;
    virtual void stop() override;
    virtual void ping() override;

    virtual void sendKeepAliveMessages();
    virtual void sendBroadcastMessage(const std::string &sender, const std::vector<uint8_t> &message);
    virtual void sendDirectMessage(const std::string &sender, const std::string &receiver, const std::vector<uint8_t> &message);
    virtual void sendLoginMessage(const std::string &newUsername, const std::string &oldUsername);
    virtual void sendUserConnectionStateChangedMessage(const std::string & username,bool isConnected);
    virtual void sendConnectedUsersMessage(const std::string & connectedUsers, std::shared_ptr<ITCPConnection> connection);
    virtual void sendServerLogsMessage(const std::string & log);


    std::string getServerName();
    std::string getConnectedUsers();

private:
    void startListening();
    void onAcceptCallback(const asio::error_code &error);
    void onMessageCallback(uint8_t *data, size_t dataLen, std::shared_ptr<ITCPConnection> connection);

    void onUndefinedMessageCallback(std::shared_ptr<ITCPConnection> connection);
    void onKeepAliveMessageCallback(std::shared_ptr<ITCPConnection> connection);
    void onLoginMessageCallback(const std::string &newUsername,const std::string &oldUsername, std::shared_ptr<ITCPConnection> connection);
    void onBroadcastMessageCallback(const std::string &sender, const std::vector<uint8_t> &message, std::shared_ptr<ITCPConnection> connection);
    void onDirectMessageCallback(const std::string &sender, const std::string &receiver, const std::vector<uint8_t> &message, std::shared_ptr<ITCPConnection> connection);
    void onConnectedUsersMessageCallback(const std::string &connectedUsers, std::shared_ptr<ITCPConnection> connection);

    void processEndianness(uint32_t &header);
    void sendMessageToAllClients(const std::vector<uint8_t> & message);
    std::shared_ptr<ITCPConnection> getClientConnection(ClientInfo client);

private:
    std::string m_serverName = "Server";
    std::string m_defaultUserName = "Unnamed";

    std::shared_ptr<IAsioContext> m_context;
    std::shared_ptr<IAsioSocket> m_socket;
    std::shared_ptr<IAsioAcceptor> m_acceptor;
    std::shared_ptr<INetworkFactory> m_networkFactory;

    std::shared_ptr<ISerializer> m_serializer;
    std::shared_ptr<IResolver> m_resolver;
    std::shared_ptr<IEncryptor> m_encryptor;

    std::unordered_map<std::shared_ptr<ITCPConnection>, ClientInfo> m_clients;
};
