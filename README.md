# Lightweight Client-Server
## 1. What is it?
It is a simple and lightweight instant messaging application based on client-server architecture and custom messages protocol. Project allows multiple users to communicate through the server, which can be deployed on any online computer with static ip. Application supports sending direct and broadcast text messages with fully encrypted content.  

## 2. Features
- Simple lightweight implementation
- Low messages overhead
- Data is encrypted by 128bit AES
- Custom messages protocol based on Google Protocol Buffers
- Support direct and broadcast text message 
- Direct messages are unreadable for server (encrypted with a different key). Content is restricted only for endpoints.
- Nicknames management. Nickname changing is avalible, list of connected users can be displayed.


## 3. Application layers

Acording to TCP/IP model layers, project is located in the aplication layer which can be splitted into several smaller layers.

![alt text](doc/layers.png "Layers")

#### Common Layers

- <b> Asynchronous Network Library </b>

This layer is based on <b> Boost::Asio </b> network library which is open-source and widely used for network programming in C++ environment. It provides asynchronous interface to operational system networking resources and take andvantege of platform independence. This layer has been extended with interfaces for network primitives (sockets, context, resolver, acceptor etc.) for testing purposes. 

- <b> Data Frame Synchronization </b>

Synchronization layer is made for preceisly extracting messages from bytestream. It wraps up messages with simple header  which consists of start flag and message size. <b> TCPConnection </b> class reads header first and based on header's information it determines how many bytes it should receive to read whole message.

- <b> Network Control Header Encryption </b>

Encryption layers is resposinble for upper layer data encryption using <b>  128-bit AES Algorithm</b>. Encryption is processed with public known key because every client should be able to interpret control messages.

- <b> Network Control Header Serialization </b>

This layer is based on <b> Google Protocol Buffers </b> which are responsible for control header serialization, which consists of message type, sender and receiver. This control header can be used without upper layers message, just to send control information(login change, keep alive etc).

#### Client Side Specific Layers

- <b> Message Encryption </b>

Message encryption layer is similar to header encryption layer with the difference of another encryption key which can be modified by connected users(TODO). 

- <b> Message Serialization </b>

Message Serialization layer is resposinble for message type serialization. At the momment project supports text messages but in the future it will support image and audio messages.

## 4. Currently supported commands

 <table >
  <tr>
    <th>Command</th>
    <th>Content</th>
    <th>Description</th>
  </tr>
  <tr>
    <td>bm</td>
    <td>[Message]</td>
    <td>Send broadcast message.</td>
  </tr>
  <tr>
    <td>dm</td>
    <td>[User] [Message]</td>
    <td>Send direct message to specified user</td>
  </tr>
    <tr>
    <td>lm</td>
    <td>[New username]</td>
    <td >Request the server to change the username</td>
  </tr>
    <tr>
    <td>cu</td>
    <td>-</td>
    <td>Request the server for a users list</td>
  </tr>
</table> 

## 5. Technologies
- Written in C++
- Asio Library
- Google Protocol Buffers
- Google Test
- Google Mock
<!--- 
- Qt
--->


## 6. Getting Started
### Installation

#### Option 1: Run with docker container.
```
$ sudo docker run -it -p 50000:50000 registry.gitlab.com/lightweightchat/chat-server:latest 50000
```
The method above runs a container form the production container registry.  
A production contiainer has entrypoint setted to LightweightServer, argument passed to entrypoint is a port number.
#### Option 2: From sources
```
$ git clone https://gitlab.com/lightweightchat/chat-server
$ git submodule init
$ git submodule update
$ mkdir build
$ cd build
$ cmake ..
$ make all
```
#### Option 3: From sources, manage dependencies with Docker
```
$ git clone link
$ git submodule init
$ git submodule update
$ mkdir build
$ docker build -t registry.gitlab.com/lightweightchat/chat-server:latest .
$ docker run -it --volume ${PWD}/build:/build
$ cd build
$ cmake ..
$ make all
```

## 7. Usage Examples

<table align="center"><tr>
<td align="center" width="1200">
<img src="doc/examples.mp4"  align="center" width="1200" height="600">
</td>
</tr>
<tr>
<td align="center">
[video containing example of use](doc/examples.mp4).
</td>
</tr>
</table>

## 7. Licence
MIT Licence
