#pragma once

#include "gmock/gmock.h"
#include "gtest/gtest.h"

#include <memory>
#include <string>

#include "IAsioAcceptorMock.h"
#include "IAsioContextMock.h"
#include "IAsioSocketMock.h"
#include "NetworkFactoryMock.h"
#include "HeaderResolverMock.h"
#include "HeaderSerializerMock.h"
#include "server.h"
#include "EncryptorMock.h"
#include "tcpConnectionMock.h"

using namespace ::testing;
using ::testing::Pointee;

class ServerTestFixture : public testing::Test
{
public:
    static constexpr IP ip = IP::V4;
    static constexpr size_t port = 5000;


    void AddTwoStandardConnections();
    void triggerAcceptorCallback(int n = 1);
    void triggerAcceptorCallbackWithError(int n = 1);

    void SetUp()
    {
        fakeClientInfo = std::make_shared<NiceMock<ClientInfo>>("Unnamed0\n", "0.0.0.0");
        contextMock = std::make_shared<NiceMock<AsioContextMock>>();
        socketMock = std::make_shared<NiceMock<AsioSocketMock>>();
        acceptorMock = std::make_shared<NiceMock<AsioAcceptorMock>>();
        tcpConnectionMock = std::make_shared<NiceMock<TCPConnectionMock>>();
        tcpConnectionSecondMock = std::make_shared<NiceMock<TCPConnectionMock>>();
        networkFactoryMock = std::make_shared<NiceMock<NetworkFactoryMock>>();
        protoSerializerMock = std::make_shared<NiceMock<HeaderSerializerMock>>();
        protoResolverMock = std::make_shared<NiceMock<HeaderResolverMock>>();
        encryprotMock = std::make_shared<NiceMock<EncryptorMock>>();

        ON_CALL(*networkFactoryMock, makeContext()).WillByDefault(Return(contextMock));
        ON_CALL(*networkFactoryMock, makeSocket(testing::_)).WillByDefault(Return(socketMock));
        ON_CALL(*networkFactoryMock, makeAcceptor(testing::_, testing::_, testing::_)).WillByDefault(Return(acceptorMock));
        ON_CALL(*networkFactoryMock, makeTCPConnection(testing::_)).WillByDefault(Return(tcpConnectionMock));
        
        ON_CALL(*protoResolverMock, setBroadcastHeaderCallback(testing::_)).WillByDefault(Invoke([this](auto bmCallback){
            capturedBMCallback = bmCallback;
            }));

        ON_CALL(*protoResolverMock, setLoginHeaderCallback(testing::_)).WillByDefault(Invoke([this](auto loginCallback){
            capturedLoginCallback = loginCallback;
            }));

        ON_CALL(*protoResolverMock, setDirectHeaderCallback(testing::_)).WillByDefault(Invoke([this](auto dmCallback){
            capturedDMCallback = dmCallback;
            }));

        ON_CALL(*protoResolverMock, setConnectedUsersCallback(testing::_)).WillByDefault(Invoke([this](auto connectedUsersCallback){
            capturedConnectedUsersCallback = connectedUsersCallback;
            }));

        

        serverTCP = new Server(serverName, IP::V4, 5000, networkFactoryMock,protoSerializerMock,protoResolverMock,encryprotMock);
    }

    void TearDown()
    {
        delete serverTCP;
    }

public:
    std::string serverName = "Server";
    std::string testString = "Test String";
    std::string testSender = "testSender";
    std::string testReceiver = "testReceiver";
    std::string defaultUsernameFirst = "Unnamed0";
    std::string defaultUsernameSecond = "Unnamed1";
    std::string testConnectedUsers = "User1 User2 User3 User4";


    std::shared_ptr<ClientInfo> fakeClientInfo;

    std::shared_ptr<NiceMock<AsioContextMock>> contextMock;
    std::shared_ptr<NiceMock<AsioSocketMock>> socketMock;
    std::shared_ptr<NiceMock<AsioAcceptorMock>> acceptorMock;
    std::shared_ptr<NiceMock<TCPConnectionMock>> tcpConnectionMock;
    std::shared_ptr<NiceMock<TCPConnectionMock>> tcpConnectionSecondMock;
    std::shared_ptr<NiceMock<NetworkFactoryMock>> networkFactoryMock;
    std::shared_ptr<NiceMock<HeaderSerializerMock>> protoSerializerMock;
    std::shared_ptr<NiceMock<HeaderResolverMock>> protoResolverMock;
    std::shared_ptr<NiceMock<EncryptorMock>> encryprotMock;
    IResolver::BroadcastHeaderCallback_t capturedBMCallback; 
    IResolver::LoginHeaderCallback_t capturedLoginCallback; 
    IResolver::DirectHeaderCallback_t capturedDMCallback; 
    IResolver::ConnectedUsersHeaderCallback_t capturedConnectedUsersCallback;

    Server *serverTCP;
};

void ServerTestFixture::AddTwoStandardConnections()//Add two Clients to server and fake sending message from one of them
{
    EXPECT_CALL(*networkFactoryMock, makeTCPConnection(testing::_)).WillOnce(Return(tcpConnectionMock)).WillOnce(Return(tcpConnectionSecondMock));
    // EXPECT_CALL(*tcpConnectionMock, Start(testing::_, testing::_)); //adding first first client
    triggerAcceptorCallback(2);
}

void ServerTestFixture::triggerAcceptorCallback(int n)
{
    auto& exp1 = EXPECT_CALL(*acceptorMock, asyncAccept(testing::_, testing::_)).Times(n).WillRepeatedly(Invoke([&](IAsioSocket::ptr s, AcceptCallback callback) {
                                                                            callback(std::error_code());
                                                                        }));
    EXPECT_CALL(*acceptorMock, asyncAccept(testing::_, testing::_)).Times(1).After(exp1).WillOnce(Return()).RetiresOnSaturation();         
}

void ServerTestFixture::triggerAcceptorCallbackWithError(int n)
{
    auto& exp1 = EXPECT_CALL(*acceptorMock, asyncAccept(testing::_, testing::_)).Times(n).WillRepeatedly(Invoke([&](IAsioSocket::ptr s, AcceptCallback callback) {
                                                                            callback(std::error_code(1, std::generic_category()));
                                                                        }));
    EXPECT_CALL(*acceptorMock, asyncAccept(testing::_, testing::_)).Times(1).After(exp1).WillOnce(Return()).RetiresOnSaturation();  
}