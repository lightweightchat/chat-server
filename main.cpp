#include "NetworkFactory.h"
#include "asio.hpp"
#include "server.h"
#include <iostream>
#include <memory>

int main(int argc, char *argv[])
{
    if (argc < 2)
    {
        std::cerr<<"You haven't specified Server port as a command line argument!" << std::endl;
        return(1);
    }
    int serverPort;
    try{
        serverPort = std::stoi(argv[1]);
    }
    catch(const std::exception& e){
        std::cerr<<"You've specified server port in wrong format!" << std::endl;
        return(1);
    }


    std::shared_ptr<NetworkFactory> factory;
    factory = std::make_shared<NetworkFactory>();


    Server server("LightweightServer", IP::V4, serverPort, factory);

    server.setErrorCallback([](std::error_code ec) {
        std::cout << "Error occured\n"<< std::endl;
    });

    server.setConnectionCallback([&server](ClientInfo clientInfo) {
        server.sendUserConnectionStateChangedMessage(clientInfo.m_username,true);
        std::cout<<"New user connected name: "<<clientInfo.m_username<< std::endl;
    });

    server.setDisconnectCallback([&server](ClientInfo clientInfo) {
        server.sendUserConnectionStateChangedMessage(clientInfo.m_username,false);
            std::cout<<"New user disconnected name: "<<clientInfo.m_username<< std::endl;
    });

    std::thread thread_object([&]() {
        server.start();
    });
    std::string userInput;
    while(1){
        getline(std::cin, userInput);
        if (userInput=="exit")
        {
            server.stop();
            thread_object.join();
            std::cout << "Exit!" << std::endl;
            return 0;
        }
    }
}