#include "server.h"

Server::Server(std::string serverName, IP version, int port, std::shared_ptr<INetworkFactory> networkFactory, std::shared_ptr<ISerializer> serializer, std::shared_ptr<IResolver> resolver, std::shared_ptr<IEncryptor> encryptor)
    : m_serverName(serverName)
    , m_networkFactory(std::move(networkFactory))
{
    m_context = m_networkFactory->makeContext();
    m_acceptor = m_networkFactory->makeAcceptor(m_context, version, port);

    serializer ? m_serializer = serializer : m_serializer = std::make_shared<HeaderSerializer>();
    resolver ? m_resolver = resolver : m_resolver = std::make_shared<HeaderResolver>();
    encryptor ? m_encryptor = encryptor : m_encryptor = std::make_shared<Encryptor>(std::make_shared<AES>(AESKeyLength::AES_128));

    if (m_resolver)
    {
        m_resolver->setKeepAliveHeaderCallback(std::bind(&Server::onKeepAliveMessageCallback, this, std::placeholders::_1));
        m_resolver->setUndefinedHeaderCallback(std::bind(&Server::onUndefinedMessageCallback, this, std::placeholders::_1));
        m_resolver->setLoginHeaderCallback(std::bind(&Server::onLoginMessageCallback, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3));
        m_resolver->setBroadcastHeaderCallback(std::bind(&Server::onBroadcastMessageCallback, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3));
        m_resolver->setDirectHeaderCallback(std::bind(&Server::onDirectMessageCallback, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3, std::placeholders::_4));
        m_resolver->setConnectedUsersCallback(std::bind(&Server::onConnectedUsersMessageCallback, this, std::placeholders::_1, std::placeholders::_2));
    }
}

void Server::start()
{
    startListening();
    m_context->run();
}

void Server::stop()
{
    m_acceptor->close();

    for (auto &client : m_clients)
    {
        client.first->Stop();
    }
    
    m_clients.clear();
    m_socket->close();
}

void Server::ping()
{
}

std::string Server::getServerName()
{
    return m_serverName;
}

std::string Server::getConnectedUsers()
{
    std::string connectedUsers;

    for (auto &client : m_clients)
    {
        connectedUsers += client.second.m_username;
        connectedUsers += '\n';
    }

    return connectedUsers;
}

void Server::startListening()
{
    m_socket = m_networkFactory->makeSocket(m_context);

    m_acceptor->asyncAccept(m_socket, std::bind(&Server::onAcceptCallback, this, std::placeholders::_1));
}

void Server::sendKeepAliveMessages()
{
    //TODO
    // iterate through all clients and send them keep alive messages
}

void Server::sendBroadcastMessage(const std::string &sender, const std::vector<uint8_t> &message)
{
    std::vector<uint8_t> broadcastMessageHeader;
    m_serializer->makeBroadcastHeader(broadcastMessageHeader, sender);
    m_encryptor->encrypt(broadcastMessageHeader);
    uint32_t headerSize = broadcastMessageHeader.size();
    processEndianness(headerSize);

    auto ptr = reinterpret_cast<uint8_t *>(&headerSize);
    std::vector<uint8_t> broadcastMessage(ptr, ptr + sizeof(headerSize));

    broadcastMessage.insert(broadcastMessage.end(), std::make_move_iterator(broadcastMessageHeader.begin()), std::make_move_iterator(broadcastMessageHeader.end()));
    broadcastMessage.insert(broadcastMessage.end(), std::make_move_iterator(message.begin()), std::make_move_iterator(message.end()));

    sendMessageToAllClients(broadcastMessage);

}

void Server::sendDirectMessage(const std::string &sender, const std::string &receiver, const std::vector<uint8_t> &message)
{
    auto receiverConnection = getClientConnection(ClientInfo(receiver));

    if (receiverConnection)
    {
        std::vector<uint8_t> directMessageHeader;
        m_serializer->makeDirectHeader(directMessageHeader, sender, receiver);
        m_encryptor->encrypt(directMessageHeader);
        uint32_t headerSize = directMessageHeader.size();
        processEndianness(headerSize);

        auto ptr = reinterpret_cast<uint8_t *>(&headerSize);
        std::vector<uint8_t> directMessage(ptr, ptr + sizeof(headerSize));
        directMessage.insert(directMessage.end(), std::make_move_iterator(directMessageHeader.begin()), std::make_move_iterator(directMessageHeader.end()));
        directMessage.insert(directMessage.end(), std::make_move_iterator(message.begin()), std::make_move_iterator(message.end()));

        receiverConnection->Post(directMessage.data(), directMessage.size());
    }
}
void Server::sendLoginMessage(const std::string &newUsername, const std::string &oldUsername)
{
    std::vector<uint8_t> loginHeader;
    m_serializer->makeLoginHeader(loginHeader, newUsername, oldUsername);
    m_encryptor->encrypt(loginHeader);
    uint32_t headerSize = loginHeader.size();
    processEndianness(headerSize);

    auto ptr = reinterpret_cast<uint8_t *>(&headerSize);
    std::vector<uint8_t> loginBroadcastMessage(ptr, ptr + sizeof(headerSize));
    loginBroadcastMessage.insert(loginBroadcastMessage.end(), std::make_move_iterator(loginHeader.begin()), std::make_move_iterator(loginHeader.end()));

    sendMessageToAllClients(loginBroadcastMessage);
}

void Server::sendUserConnectionStateChangedMessage(const std::string &username, bool isConnected)
{
    std::vector<uint8_t> disconnectedHeaderV;
    m_serializer->makeUserConnectionStateChangedHeader(disconnectedHeaderV, username, isConnected);
    m_encryptor->encrypt(disconnectedHeaderV);
    uint32_t headerSize = disconnectedHeaderV.size();
    processEndianness(headerSize);

    auto ptr = reinterpret_cast<uint8_t *>(&headerSize);
    std::vector<uint8_t> disconnectedBroadcastMessage(ptr, ptr + sizeof(headerSize));
    disconnectedBroadcastMessage.insert(disconnectedBroadcastMessage.end(), std::make_move_iterator(disconnectedHeaderV.begin()), std::make_move_iterator(disconnectedHeaderV.end()));

    sendMessageToAllClients(disconnectedBroadcastMessage);

}

void Server::sendConnectedUsersMessage(const std::string & connectedUsers, std::shared_ptr<ITCPConnection> connection)
{
    std::vector<uint8_t> connectedUsersHeader;
    m_serializer->makeConnectedUsersHeader(connectedUsersHeader, connectedUsers);
    m_encryptor->encrypt(connectedUsersHeader);
    uint32_t headerSize = connectedUsersHeader.size();
    processEndianness(headerSize);

    auto ptr = reinterpret_cast<uint8_t *>(&headerSize);
    std::vector<uint8_t> message(ptr, ptr + sizeof(headerSize));

    message.insert(message.end(), std::make_move_iterator(connectedUsersHeader.begin()), std::make_move_iterator(connectedUsersHeader.end()));
    
    connection->Post(message.data(), message.size());
}

void Server::sendServerLogsMessage(const std::string & log)
{
    std::vector<uint8_t> logHeader;
    m_serializer->makeServerLogHeader(logHeader, log);
    m_encryptor->encrypt(logHeader);
    uint32_t headerSize = logHeader.size();
    processEndianness(headerSize);
    auto ptr = reinterpret_cast<uint8_t *>(&headerSize);
    std::vector<uint8_t> message(ptr, ptr + sizeof(headerSize));

    message.insert(message.end(), std::make_move_iterator(logHeader.begin()), std::make_move_iterator(logHeader.end()));

    sendMessageToAllClients(message);
}

void Server::onAcceptCallback(const asio::error_code &error)
{
    if (error == asio::error::operation_aborted)
    {
        return; //just normal/soft exit
    }
    if (!error)
    {
        auto newConnection = std::shared_ptr<ITCPConnection>(m_networkFactory->makeTCPConnection(m_socket));
        auto newClient = ClientInfo(m_defaultUserName + std::to_string(m_clients.size()));

        m_clients.emplace(newConnection, newClient);

        if (m_connectionCallback)
        {
            m_connectionCallback(newClient);
        }

        newConnection->Start(
            [this, newConnection](uint8_t *data, size_t dataLen) {
                onMessageCallback(data, dataLen, newConnection);
            },
            [&, connectionWeak = std::weak_ptr(newConnection)](asio::error_code ec) {
                auto client = connectionWeak.lock();
                if (client)
                {
                    auto clientInfoToDelete = m_clients[client];
                    m_clients.erase(client);
                    if (m_disconnectCallback)
                    {
                        m_disconnectCallback(clientInfoToDelete);
                    }
                }
            });
    }

    startListening();
}

void Server::onMessageCallback(uint8_t *data, size_t dataLen, std::shared_ptr<ITCPConnection> connection)
{
    uint32_t controlHeaderLen = *reinterpret_cast<uint32_t *>(data);
    processEndianness(controlHeaderLen);
    
    uint8_t *controlDataStart = data + sizeof(uint32_t);
    uint8_t *clientDataStart = controlDataStart + controlHeaderLen;
    std::vector<uint8_t> controlDataV(controlDataStart, clientDataStart);
    std::vector<uint8_t> clientV(clientDataStart, data + dataLen);
    m_encryptor->decrypt(controlDataV);
    m_resolver->parseHeader(controlDataV, clientV, connection);
}

void Server::onUndefinedMessageCallback(std::shared_ptr<ITCPConnection> connection)
{
    //TODO
    // register messages and disconnect client when messages exceeded certain number
}

void Server::onKeepAliveMessageCallback(std::shared_ptr<ITCPConnection> connection)
{
    //TODO
    // register messages and disconnect client when messages exceeded certain number
}

void Server::onLoginMessageCallback(const std::string &newUsername, const std::string &oldUsername, std::shared_ptr<ITCPConnection> connection)
{
    if (!getClientConnection(ClientInfo(newUsername)))
    {
        std::string oldUsername = m_clients[connection].m_username;
        m_clients[connection].m_username = newUsername;
        sendLoginMessage(newUsername, oldUsername);
    }
}

void Server::onBroadcastMessageCallback(const std::string &sender, const std::vector<uint8_t> &message, std::shared_ptr<ITCPConnection> connection)
{
    sendBroadcastMessage(m_clients[connection].m_username, message); 
}

void Server::onDirectMessageCallback(const std::string &sender, const std::string &receiver, const std::vector<uint8_t> &message, std::shared_ptr<ITCPConnection> connection)
{
    sendDirectMessage(m_clients[connection].m_username, receiver, message);
}

void Server::onConnectedUsersMessageCallback(const std::string &connectedUsers, std::shared_ptr<ITCPConnection> connection)
{
    std::string clients;

    for (auto &connection : m_clients)
    {
        clients += (connection.second.m_username + " ");
    }

    sendConnectedUsersMessage(clients, connection);
}

void Server::processEndianness(uint32_t &header)
{
    if (std::endian::native == std::endian::big)
    {
        header = __builtin_bswap32(header);
    }
}

void Server::sendMessageToAllClients(const std::vector<uint8_t> & message)
{
    for (auto &client : m_clients)
    {
        client.first->Post(message.data(), message.size());
    }
}

std::shared_ptr<ITCPConnection> Server::getClientConnection(ClientInfo client)
{
    for (auto &connection : m_clients)
    {
        if (connection.second.m_username == client.m_username)
        {
            return connection.first;
        }
    }

    return nullptr;
}