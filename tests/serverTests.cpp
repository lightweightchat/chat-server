#include "serverTests.h"

TEST_F(ServerTestFixture, Server_Should_Delete_Injected_Dependencies)
{
    auto contextMock_ = std::make_shared<NiceMock<AsioContextMock>>();
    auto acceptorMock_ = std::make_shared<NiceMock<AsioAcceptorMock>>();
    auto networkFactoryMock_ = std::make_shared<NiceMock<NetworkFactoryMock>>();
    auto protoSerializerMock_ = std::make_shared<NiceMock<HeaderSerializerMock>>();
    auto protoResolverMock_ = std::make_shared<NiceMock<HeaderResolverMock>>();

    ON_CALL(*networkFactoryMock_, makeContext()).WillByDefault(Return(contextMock_));
    ON_CALL(*networkFactoryMock_, makeAcceptor(testing::_, testing::_, testing::_)).WillByDefault(Return(acceptorMock_));

    std::shared_ptr<IServer> server = std::make_shared<Server>(serverName, ip, port, networkFactoryMock_, protoSerializerMock_, protoResolverMock_);

    EXPECT_CALL(*acceptorMock_, Die()).Times(1);
    EXPECT_CALL(*contextMock_, Die()).Times(1);
    EXPECT_CALL(*networkFactoryMock_, Die()).Times(1);
    EXPECT_CALL(*protoSerializerMock_, Die()).Times(1);
    EXPECT_CALL(*protoResolverMock_, Die()).Times(1);
}

TEST_F(ServerTestFixture, Server_Should_Return_Its_Name)
{
    EXPECT_STREQ(serverName.c_str(), serverTCP->getServerName().c_str());
}

TEST_F(ServerTestFixture, Server_Should_List_All_Connected_Clients)
{
    EXPECT_STREQ("", serverTCP->getConnectedUsers().c_str());
    EXPECT_CALL(*networkFactoryMock, makeTCPConnection(testing::_)).Times(2).WillRepeatedly(Invoke([]() {
        return std::make_shared<NiceMock<TCPConnectionMock>>();
    }));
    triggerAcceptorCallback(2);

    serverTCP->start();

    EXPECT_STREQ((defaultUsernameSecond + "\n" + defaultUsernameFirst + "\n").c_str(), serverTCP->getConnectedUsers().c_str());
}

TEST_F(ServerTestFixture, Server_Should_Create_Context_And_Acceptor_On_Construction)
{
    auto networkFactoryMock_ = std::make_shared<NiceMock<NetworkFactoryMock>>();
    EXPECT_CALL(*networkFactoryMock_, makeContext()).Times(1);
    EXPECT_CALL(*networkFactoryMock_, makeAcceptor(testing::_, testing::_, testing::_)).Times(1);

    auto testServer = new Server(serverName, ip, port, networkFactoryMock_);

    delete testServer;
}

TEST_F(ServerTestFixture, Server_Should_Run_Context_On_Start)
{
    EXPECT_CALL(*contextMock, run()).Times(1);

    serverTCP->start();
}

TEST_F(ServerTestFixture, Server_Should_Create_New_Socket_When_Start_Listening)
{
    EXPECT_CALL(*networkFactoryMock, makeSocket(testing::_)).Times(1);

    serverTCP->start();
}

TEST_F(ServerTestFixture, Server_Should_Start_Accepting_Connection_On_Start)
{
    EXPECT_CALL(*acceptorMock, asyncAccept(testing::_, testing::_)).Times(1);

    serverTCP->start();
}

TEST_F(ServerTestFixture, Server_Should_Remove_All_Connections_On_Stop)
{
    auto mockConnection1 = std::make_shared<NiceMock<TCPConnectionMock>>();
    auto mockConnection2 = std::make_shared<NiceMock<TCPConnectionMock>>();

    EXPECT_CALL(*networkFactoryMock, makeTCPConnection(testing::_)).WillOnce(Return(mockConnection1)).WillOnce(Return(mockConnection2));
    EXPECT_CALL(*mockConnection1, Stop()).Times(1);
    EXPECT_CALL(*mockConnection2, Stop()).Times(1);
    EXPECT_CALL(*acceptorMock, close()).Times(1);
    triggerAcceptorCallback(2);

    serverTCP->start();
    serverTCP->stop();
}

TEST_F(ServerTestFixture, Server_Should_Create_New_TCP_Connection_On_Accept)
{
    EXPECT_CALL(*networkFactoryMock, makeTCPConnection(testing::_)).Times(1);
    triggerAcceptorCallback();

    serverTCP->start();
}

TEST_F(ServerTestFixture, Server_Should_Start_TCP_Connection_On_Accept)
{
    EXPECT_CALL(*tcpConnectionMock, Start(testing::_, testing::_)).Times(1);
    triggerAcceptorCallback();

    serverTCP->start();
}

TEST_F(ServerTestFixture, Server_Should_Retrigger_Async_Accepting_After_Connection)
{
    triggerAcceptorCallback();

    serverTCP->start();
}

TEST_F(ServerTestFixture, Server_Should_Create_New_Socket_On_New_Connection)
{
    EXPECT_CALL(*networkFactoryMock, makeSocket(testing::_)).Times(2);
    triggerAcceptorCallback();

    serverTCP->start();
}

TEST_F(ServerTestFixture, Server_Should_Not_Take_Any_Action_When_Error_Occured_On_Accept)
{
    EXPECT_CALL(*networkFactoryMock, makeTCPConnection(testing::_)).Times(0);
    EXPECT_CALL(*tcpConnectionMock, Start(testing::_, testing::_)).Times(0);
    triggerAcceptorCallbackWithError();

    serverTCP->start();
}

TEST_F(ServerTestFixture, Server_Should_Execute_Callback_When_Client_Disconnect)
{
    EXPECT_CALL(*tcpConnectionMock, Start(testing::_, testing::_)).WillOnce(Invoke([this](onMessageCallback messageHandler, onErrorCallback errorHandler) {
        errorHandler(std::error_code());
    }));
    serverTCP->setDisconnectCallback([this](ClientInfo clientInfo) {
        EXPECT_STREQ(clientInfo.m_username.c_str(), defaultUsernameFirst.c_str());
    });
    triggerAcceptorCallback();

    serverTCP->start();
}

TEST_F(ServerTestFixture, Server_Should_Execute_Callback_When_Client_Connect)
{
    EXPECT_CALL(*tcpConnectionMock, Start(testing::_, testing::_)).WillOnce(Invoke([this](onMessageCallback messageHandler, onErrorCallback errorHandler) {
        errorHandler(std::error_code());
    }));
    serverTCP->setConnectionCallback([this](ClientInfo clientInfo) {
        EXPECT_STREQ(clientInfo.m_username.c_str(), defaultUsernameFirst.c_str());
    });
    triggerAcceptorCallback();

    serverTCP->start();
}

TEST_F(ServerTestFixture, Server_Should_Remove_Client_Connection_Pointer_When_Error_Occured)
{
    auto mockConnection1 = std::make_shared<NiceMock<TCPConnectionMock>>();
    auto mockConnection2 = std::make_shared<NiceMock<TCPConnectionMock>>();

    EXPECT_CALL(*networkFactoryMock, makeTCPConnection(testing::_)).WillOnce(Return(mockConnection1)).WillOnce(Return(mockConnection2));
    EXPECT_CALL(*mockConnection1, Start(testing::_, testing::_)).WillOnce(Invoke([this](onMessageCallback messageHandler, onErrorCallback errorHandler) {
        errorHandler(std::error_code(1, std::generic_category()));
    }));
    EXPECT_CALL(*mockConnection1, Die()).Times(1);

    triggerAcceptorCallback(2);
    serverTCP->start();
}

//every client should receive headersize|headerSerializer|message
TEST_F(ServerTestFixture, Server_Should_Broadcast_Messages_When_Boradcast_Message_Received)
{
    AddTwoStandardConnections(); //Make server to have 2 clients
    //Prepare partial message data
    std::vector<uint8_t> ResponseHeader(8, 1); //fake header constructed by sever's headerSerializer
    uint32_t headerSize = ResponseHeader.size();
    std::vector<uint8_t> fakeMessage(10, 5);
    //prepare message that clent will receive
    auto ptr = reinterpret_cast<uint8_t *>(&headerSize);
    std::vector<uint8_t> clientMessage(ptr, ptr + sizeof(headerSize));
    clientMessage.insert(clientMessage.end(), ResponseHeader.begin(), ResponseHeader.end());
    clientMessage.insert(clientMessage.end(), fakeMessage.begin(), fakeMessage.end());

    //Make serializer to return fake header as a result of server request to it.
    EXPECT_CALL(*protoSerializerMock, makeBroadcastHeader(testing::_, defaultUsernameSecond)).Times(1).WillOnce(testing::Invoke([&](std::vector<uint8_t> &serializedMsgOut, const std::string &sender) {
        serializedMsgOut.assign(ResponseHeader.begin(), ResponseHeader.end());
        return true;
    }));

    //Real test expectiations clients should receive : headersize|headerSerializer|message
    EXPECT_CALL(*tcpConnectionMock, Post(Pointee(*clientMessage.data()), clientMessage.size())).Times(1);
    EXPECT_CALL(*tcpConnectionSecondMock, Post(Pointee(*clientMessage.data()), clientMessage.size())).Times(1);

    serverTCP->start();
    capturedBMCallback("sender ignored due to fake control header", fakeMessage, tcpConnectionSecondMock);
}

TEST_F(ServerTestFixture, Server_Should_Change_Person_Username_When_Login_Message_Received)
{
    AddTwoStandardConnections();
    serverTCP->start();
    capturedLoginCallback("NewLogin1", "OldLogin(Ignored, server takes old login form internal resources", tcpConnectionMock);
    capturedLoginCallback("NewLogin2", "OldLogin(Ignored, server takes old login form internal resources", tcpConnectionSecondMock);
    EXPECT_STREQ("NewLogin2\nNewLogin1\n", serverTCP->getConnectedUsers().c_str());
}

TEST_F(ServerTestFixture, Server_Should_Send_Notification_When_Login_Is_Changed)
{
    std::vector<uint8_t> ResponseHeader(8, 1); //fake header constructed by sever's headerSerializer
    uint32_t headerSize = ResponseHeader.size();

    //prepare message that clent will receive
    auto ptr = reinterpret_cast<uint8_t *>(&headerSize);
    std::vector<uint8_t> clientMessage(ptr, ptr + sizeof(headerSize));
    clientMessage.insert(clientMessage.end(), ResponseHeader.begin(), ResponseHeader.end());

    // expect that server will use serializer with proper arguments to compre
    EXPECT_CALL(*protoSerializerMock, makeLoginHeader(testing::_, "NewLogin1", defaultUsernameFirst)).Times(1).WillOnce(testing::Invoke([&](auto &serializedMsgOut, const auto &newLogin, const auto &oldLogin) {
        serializedMsgOut.assign(ResponseHeader.begin(), ResponseHeader.end());
        return true;
    }));

    EXPECT_CALL(*tcpConnectionMock, Post(Pointee(*clientMessage.data()), clientMessage.size())).Times(1);
    EXPECT_CALL(*tcpConnectionSecondMock, Post(Pointee(*clientMessage.data()), clientMessage.size())).Times(1);

    AddTwoStandardConnections();
    serverTCP->start();
    capturedLoginCallback("NewLogin1", "OldLogin(Ignored, server takes old login form internal resources", tcpConnectionMock);
}

TEST_F(ServerTestFixture, Server_Should_Not_Change_Person_Username_When_Username_Already_Exists)
{
    AddTwoStandardConnections();
    serverTCP->start();
    capturedLoginCallback("NewLogin1", "OldLogin(Ignored, server takes old login form internal resources", tcpConnectionMock);
    capturedLoginCallback("NewLogin2", "OldLogin(Ignored, server takes old login form internal resources", tcpConnectionSecondMock);
    capturedLoginCallback("NewLogin1", "OldLogin(Ignored, server takes old login form internal resources", tcpConnectionSecondMock); //should has no effect
    EXPECT_STREQ("NewLogin2\nNewLogin1\n", serverTCP->getConnectedUsers().c_str());
}

//specific client should receive headersize|headerSerializer|message
TEST_F(ServerTestFixture, Server_Should_Send_Message_To_Specific_Person_When_Direct_Message_Received)
{
    AddTwoStandardConnections(); //Make server to have 2 clients
    //Prepare partial message data
    std::vector<uint8_t> ResponseHeader(8, 1); //fake header constructed by sever's headerSerializer
    uint32_t headerSize = ResponseHeader.size();
    std::vector<uint8_t> fakeMessage(10, 5);
    //prepare message that clent will receive
    auto ptr = reinterpret_cast<uint8_t *>(&headerSize);
    std::vector<uint8_t> clientMessage(ptr, ptr + sizeof(headerSize));
    clientMessage.insert(clientMessage.end(), ResponseHeader.begin(), ResponseHeader.end());
    clientMessage.insert(clientMessage.end(), fakeMessage.begin(), fakeMessage.end());

    //Make serializer to return fake header as a result of server request to it.
    EXPECT_CALL(*protoSerializerMock, makeDirectHeader(testing::_, defaultUsernameFirst, defaultUsernameSecond)).Times(1).WillOnce(testing::Invoke([&](std::vector<uint8_t> &serializedMsgOut, const std::string &sender, const std::string &receiver) {
        serializedMsgOut.assign(ResponseHeader.begin(), ResponseHeader.end());
        return true;
    }));
    EXPECT_CALL(*tcpConnectionMock, Post(testing::_, testing::_)).Times(0);                                     //This user will not receive message
    EXPECT_CALL(*tcpConnectionSecondMock, Post(Pointee(*clientMessage.data()), clientMessage.size())).Times(1); //This user will receive a message
    serverTCP->start();
    capturedDMCallback("sender ignored due to fake control header", defaultUsernameSecond, fakeMessage, tcpConnectionMock);
}

TEST_F(ServerTestFixture, Server_Should_Encrypt_Broadcast_Messages_When_Encryption_Is_Enabled)
{
    std::vector<uint8_t> fakeMsg = {1, 2, 3, 4, 5};
    std::vector<uint8_t> fakeHeader = {1, 2, 3, 4, 5};

    EXPECT_CALL(*protoSerializerMock, makeBroadcastHeader(testing::_, testing::_)).Times(1).WillOnce(testing::Invoke([&](std::vector<uint8_t> &serializedMsgOut, const std::string &sender) {
        serializedMsgOut.assign(fakeHeader.begin(), fakeHeader.end());
        return true;
    }));

    AddTwoStandardConnections();

    serverTCP->start();
    EXPECT_CALL(*encryprotMock, encrypt(fakeMsg)).Times(1); //This user will receive a message
    capturedBMCallback("sender ignored due to fake control header", fakeMsg, tcpConnectionSecondMock);
}

TEST_F(ServerTestFixture, Server_Should_Encrypt_Direct_Messages_When_Encryption_Is_Enabled)
{
    std::vector<uint8_t> fakeMsg = {1, 2, 3, 4, 5};
    std::vector<uint8_t> fakeHeader = {1, 2, 3, 4, 5};

    EXPECT_CALL(*protoSerializerMock, makeDirectHeader(testing::_, testing::_, testing::_)).Times(1).WillOnce(testing::Invoke([&](std::vector<uint8_t> &serializedMsgOut, const std::string &sender, const std::string &receiver) {
        serializedMsgOut.assign(fakeHeader.begin(), fakeHeader.end());
        return true;
    }));

    AddTwoStandardConnections();

    serverTCP->start();
    EXPECT_CALL(*encryprotMock, encrypt(fakeMsg)).Times(1); //This user will receive a message
    capturedDMCallback("sender ignored due to fake control header", defaultUsernameFirst, fakeMsg, tcpConnectionSecondMock);
}

TEST_F(ServerTestFixture, Server_Should_Send_ConnectedClients_When_ConnectedClients_Request_Message_Received)
{
    std::vector<uint8_t> fakeHeader = {1, 2, 3, 4, 5};
    std::string connectedUsers;

    EXPECT_CALL(*protoSerializerMock, makeConnectedUsersHeader(testing::_, testing::_)).Times(1).WillOnce(testing::Invoke([&](std::vector<uint8_t> &serializedMsgOut, const std::string &receivedConnectedUsersList) {
        connectedUsers = receivedConnectedUsersList;
        return true;
    }));

    AddTwoStandardConnections();

    serverTCP->start();

    capturedConnectedUsersCallback("", tcpConnectionSecondMock);

    std::string testOutput = (defaultUsernameSecond + " " + defaultUsernameFirst + " ");
    ASSERT_STREQ(connectedUsers.c_str(), testOutput.c_str());

}

